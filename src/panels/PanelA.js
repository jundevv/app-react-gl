import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import { observer } from "mobx-react";
import '../../node_modules/ag-grid-community/dist/styles/ag-grid.css';
import '../../node_modules/ag-grid-community/dist/styles/ag-theme-balham-dark.css';


@observer
export default class PanelA extends Component{
  componentDidMount () {
  }

  render () {
    const columnDefs = [
      {headerName: "Make", field: "make"},
      {headerName: "Model", field: "model"},
      {headerName: "Price", field: "price"}
    ];
  
    return (
        <div className="ag-theme-balham-dark"
              style={{ height: '100%', width: '100%' }} >
            <AgGridReact
              enableSorting={true}
              enableFilter={true}
              columnDefs = {columnDefs}
              rowData = {this.props.store.rowData}>
            </AgGridReact>
        </div>
    )
  }
};


