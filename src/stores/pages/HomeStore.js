import { observable, action } from 'mobx';


class HomeStore {
  @observable open = false
  @observable selectedPanel = 'home'
  
  @action _setOpen = (open) => {this.open = open}
  @action _setSelectedPanel = (selectedPanel) => {this.selectedPanel = selectedPanel}


  @action handleDrawerOpen = (e) => {
    this._setOpen(true)
  }
  @action handleDrawerClose = () => {
    this._setOpen(false)
  }
  @action handleSelectPanel = (event, panel) => {
    this._setSelectedPanel(panel)
  }

 }

 export default new HomeStore()






        
