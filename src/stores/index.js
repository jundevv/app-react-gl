import MasterStore from './MasterStore';
import pagestores from './pages';
import panelstores from './panels';

const stores = {
  // MasterStore,
  pagestores,
  panelstores
};

export default stores;

