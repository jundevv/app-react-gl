import React from 'react';
import styles from './HomeStyle';
import Header from './Header';
import SideMenu from './SideMenu';
import PanelA from 'panels/PanelA';
import PropTypes from 'prop-types';
import { observer } from "mobx-react";
import { withStyles } from '@material-ui/core/styles';


@observer
class Home extends React.Component {
  renderPanel = (selectedPanel) => {
    const { panelstores } = this.props.store;

    if (selectedPanel==='home') {
      return (<PanelA store={panelstores.PanelAStore}/>)
    }
    else if (selectedPanel==='panela') {
      return (<PanelA store={panelstores.PanelAStore}/>)
    }
    else if (selectedPanel==='panelb') {
      return ('Fame')
    }
  }
  render() {
    const { classes, theme, store } = this.props;
    const { pagestores } = store;

    return (
          <div className={classes.root}>
            <Header classes={classes} store={pagestores.HomeStore}/>
            <SideMenu classes={classes} theme={theme} store={pagestores.HomeStore}/>
            
            <main className={classes.content}>
              <div className={classes.toolbar} />
                {this.renderPanel(pagestores.HomeStore.selectedPanel)}
            </main>
          </div>
    );
  }
}


Home.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Home);