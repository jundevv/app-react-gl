import React from 'react';
import classNames from 'classnames';
import { observer } from "mobx-react";
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import MailIcon from '@material-ui/icons/Mail';
import DeleteIcon from '@material-ui/icons/Delete';
import ReportIcon from '@material-ui/icons/Report';


@observer
export default class SideMenu extends React.Component {
  render() {
    const { classes, store, theme } = this.props;

    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !store.open && classes.drawerPaperClose),
        }}
        open={store.open}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={store.handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          <div>
            <ListItem 
              button
              selected={store.selectedPanel === 'panela'}
              onClick={event => store.handleSelectPanel(event, 'panela')}
              >
              <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Panel A" />
            </ListItem>

            <ListItem 
              button
              selected={store.selectedPanel === 'panelb'}
              onClick={event => store.handleSelectPanel(event, 'panelb')}
              >
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText primary="Panel B" />
            </ListItem>

            <ListItem button>
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="Send mail" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="Drafts" />
            </ListItem>
          </div>
        </List>
        <Divider />
          <List>
            <div>
            <ListItem button>
              <ListItemIcon>
                <MailIcon />
              </ListItemIcon>
              <ListItemText primary="All mail" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <DeleteIcon />
              </ListItemIcon>
              <ListItemText primary="Trash" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <ReportIcon />
              </ListItemIcon>
              <ListItemText primary="Spam" />
            </ListItem>
            </div>
          </List>
        </Drawer>
    )
  }
}