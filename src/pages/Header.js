
import React from 'react';
import classNames from 'classnames';
import { observer } from "mobx-react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';


@observer
export default class Header extends React.Component {
  render() {
    const { classes, store } = this.props;
    return (
      <AppBar
        position="absolute"
        className={classNames(classes.appBar, store.open && classes.appBarShift)}
      >
        <Toolbar disableGutters={!store.open}>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={store.handleDrawerOpen}
            className={classNames(classes.menuButton, store.open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="title" color="inherit" noWrap>
            Sample
          </Typography>
        </Toolbar>
      </AppBar>
    )
  }
}