import React from 'react';
import Home from './pages/Home';
import { inject } from "mobx-react";
import './App.css';


@inject('stores')
class App extends React.Component {
  render() {
    return (
      <Home store={this.props.stores}/>
    );
  }
}

export default App;