import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react'
import stores from './stores'
import './main.css';

window.React = React;
window.ReactDOM = ReactDOM;


ReactDOM.render(
  <Provider stores={stores}>
      <App />
  </Provider>, 
    document.getElementById('root')
);
